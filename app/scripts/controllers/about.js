'use strict';

/**
 * @ngdoc function
 * @name caughtApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the caughtApp
 */
angular.module('caughtApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
