'use strict';

/**
 * @ngdoc function
 * @name caughtApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the caughtApp
 */
angular.module('caughtApp')
  .controller('UsersCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
	})
.controller('GreetController', ['$scope', '$rootScope', function($scope, $rootScope) {
		$scope.name = 'Oisin';
		$rootScope.department = 'Proofpoint';
	}])
		
.controller('ListController', ['$scope', '$http', function($scope, $http) {
		$http.get('users.json').then(function (response) {
			$scope.userList = response.data.users;
		});
		$scope.save = function() {
			$http({
				method: "POST",
				url: "users.json",
				data: users.score,
				headers: { 'Content-Type': 'application/json; charset=utf-8'}
			});
		};
}]);