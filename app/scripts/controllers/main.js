'use strict';

/**
 * @ngdoc function
 * @name caughtApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the caughtApp
 */
angular.module('caughtApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
